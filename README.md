SUCCESS - Enable merge MR on pipeline success and have a successful job be required to merge the MR  
SUCCESS - ... + The pipeline was detached and for MR specifically using only keyword  
FAILED  - As explained in this issue, external CI/CD is resulting in the MR to be merable:  
https://gitlab.com/gitlab-org/gitlab/-/issues/202523
